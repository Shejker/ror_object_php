<?php

include('db.php');

$id = isSet($_GET['id']) ? intval ($_GET['id']) : 0;

if ($id>0) {

    $sth = $pdo->prepare('DELETE FROM regal WHERE id = :id');
    $sth->bindParam(':id', $id);
    $sth->execute();

    header('location: queries.php');

} else {

    header('location: queries.php');
    
}