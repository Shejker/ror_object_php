<?php

try {

$pdo = new PDO('mysql:host=localhost;dbname=ksiazki;encoding=utf8', 'root', 'onehome123');
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) {

    echo 'Error: '.$e->getMessage();
    
}