<?php

    include('session.php');

    function t1($val, $min, $max) {

        return ($val >= $min && $val <= $max);

    }

    $count = $pdo->query('SELECT COUNT(id) as cnt FROM regal')->fetch()['cnt'];

    $page = isSet($_GET['page']) ? intval ($_GET['page'] - 1) : 0;

    $limit = 5;

    $from = $page * $limit;

    $allPage = ceil($count / $limit);

    $sql = 'SELECT r.*, c.name FROM regal r LEFT JOIN category c ON r.cat_id = c.id ORDER BY r.id DESC LIMIT ' . $from . ', ' . $limit;

    $tbl = $pdo->query($sql);

    echo '<a href="add.php">Dodaj lekturę</a><br><br>';

    echo '<table border="1">';
        echo '<tr>';

            echo '<th>ID</th>';
            echo '<th>Tytuł</th>';
            echo '<th>Okładka</th>';
            echo '<th>Autor</th>';
            echo '<th>Recenzja</th>';
            echo '<th>Kategoria</th>';
            echo '<th>Opcje</th>';

        echo '</tr>';

    foreach ($tbl->fetchAll() as $value) {

        echo '<tr>';

            echo '<td>'.$value['id'].'</td>';
            echo '<td>'.$value['tytul'].'</td>';
            echo '<td>';

                    if($value['cover']) {

                        echo '<a target="_blank" href="img/' . str_replace('cover_', 'org_', $value['cover']) . '"><img src="img/' . $value['cover'] . '"></a>';

                    } else {

                        echo 'Brak okladki';
                        
                    }

            echo '</td>';
            echo '<td>'.$value['autor'].'</td>';
            echo '<td>'.$value['recenzja'].'</td>';
            echo '<td>'.$value['name'].'</td>';
            echo '<td><a href="delete.php?id='.$value['id'].'">Usuń</a> | <a href="add.php?id='.$value['id'].'">Edytuj</a></td>';

        echo '<tr>';

    }

    echo '</table>';

    if($page > 4) {
        echo '<a href="queries.php?page=1"> < pierwsza strona </a> | ';
    }

    for($i = 1; $i <= $allPage; $i++) {

        $bold = ($i == ($page + 1)) ? 'style="font-size: 24px;"' : '';


        if(t1($i, ($page -3), ($page + 5))) {

            echo '<a ' . $bold . ' href="queries.php?page=' . $i . '">' . $i . '</a> | ';

        }

    }

     if($page < ($allPage - 1)) {

        echo '<a href="queries.php?page=' . $allPage . '">ostatnia strona > </a> | ';

     }